# SBL Coding Exercise
## By Diego Malatesta <diego.malatesta@gmail.com>

### Requirements
Using the provided JSON endpoint: fetch the data and render a collection of card elements that contain the available data. This collection of cards should work appropriately across all viewport sizes from small (mobile) all the way up to very large (4K desktop display). The look and feel of each card element should resemble that of the linked Sketch Cloud document.

### Resources:

JSON endpoint: https://api.jsonbin.io/b/5e95bd3c5fa47104cea035df  
Sketch Cloud document: https://sketch.cloud/s/z54pr/v/x0jdAA/  
Roboto Condensed Font: https://fonts.google.com/specimen/Roboto+Condensed

### Implementation Notes

I hope you enjoy reading my code! :)

I bootstrapped the project with create-react-app for convenience, as I decided to use React for the test because it makes things way neater to write. I did initially consider to just write plain JS, but it would have ended up being way too verbose and the code too repetitive.
For CSS I chose to use a simple BEM convention since the project is small. On a larger scale project I could have used CSS in JS with css-loader modules or styled-components for example.

Regarding the choice about the layout, for narrow screens the cards tile up vertically and take the whole width of the screen. When going wider I decided to use a grid layout, which brought some challenges in order to display the cards with consistent dimensions. This meant that I had to trade off a bit of cropping of the images, and the title bar sometimes having a lot of whitespace compared to the others, if the title fits on one line.

I think that what I came up with is one good solution to display these cards in a vacuum, however having to display them in an actual site might have changed my approach.

To build the project locally you can use `npm start` to start a live reload server, or `npm run build` to build the production version.

I wrote one simple test with jest and react-testing-library that you can run with `npm run test`.

Thank you for your time!
Diego