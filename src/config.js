export const API_ENDPOINT = "https://api.jsonbin.io/b/5e95bd3c5fa47104cea035df";
export const STATUS_SUCCESS = 200;
export const GENERIC_API_ERROR_MESSAGE = "There was an error fetching the data from the API";
