import { API_ENDPOINT, STATUS_SUCCESS, GENERIC_API_ERROR_MESSAGE } from "../config";

/**
 * Retrieves the cards from the API endpoint.
 *
 * Note that the provided API allows for CORS (Cross Origin Resource Sharing)
 * so it's possible to just GET the data without the need of using workarounds
 * like JSONP (since I don't have access to the API server).
 *
 * @returns {Array} The list of cards. Empty in case of error.
 */
export default async function getCards() {
  const response = await fetch(API_ENDPOINT);

  if (response.ok) {
    const { status, message, items } = await response.json();

    if (status === STATUS_SUCCESS) {
      return items;
    }

    // Assuming that message is the error message.
    console.error(status, message);
  }

  // Ideally we should log the errors in another way, or at least
  // avoid logging on production.
  console.error(GENERIC_API_ERROR_MESSAGE);

  return [];
}
