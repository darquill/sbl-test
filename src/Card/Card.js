import React from "react";
import RunningTime from "../RunningTime/RunningTime";
import "./Card.css";

const Card = ({url, title, image, runningTime = null}) => {
  return (
    <article className="card__wrapper">
      <a className="card" href={url}>
        <header className="card__header">
          <img src={image} alt={title} className="card__image" />
          {runningTime && <RunningTime time={runningTime} />}
        </header>
        <h1 className="card__title">{title}</h1>
      </a>
    </article>
  );
};

export default Card;
