import React from "react";
import "./RunningTime.css";

const RunningTime = ({time}) => {
  return (
    <span className="runningTime">
        <span className="runningTime__icon" role="img" aria-label="Running Time">🕒</span>
        <span className="runningTime__text">{`${time} mins`}</span>
    </span>
  );
};

export default RunningTime;
