/**
 * A very basic test of the app.
 * Normally each component should be tested separately,
 * and each meaningful part of the component should have
 * its own test.
 **/

import React from 'react';
import { act, render, screen } from '@testing-library/react';
import App from './App';
import getCards from './api/cards';
import mockData from './__mock__/data.json';

jest.mock('./api/cards');

getCards.mockReturnValue(mockData.items);

test('renders the App correctly', async () => {
  await act(async () => {
    render(<App />);
  });

  const mainTitle = screen.getByText(/sbl coding test/i);
  const subTitle = screen.getByText(/diego malatesta/i);

  expect(mainTitle).toBeInTheDocument();
  expect(subTitle).toBeInTheDocument();
  
  for (const item of mockData.items) {
    const itemElement = screen.getByText(item.title);
    expect(itemElement).toBeInTheDocument();
  }
});
