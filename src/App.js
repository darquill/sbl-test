import React, { useState, useEffect } from "react";
import getCards from "./api/cards";
import Card from "./Card/Card";
import './App.css';

const App = () => {
  const [cards, setCards] = useState([]);

  useEffect(() => {
    async function updateCards() {
        const cards = await getCards();
        setCards(cards);
    }

    updateCards();
  }, []);

  return (
    <>
      <header className="app__header">
        <h2 className="app__title">SBL Coding Test</h2>
        <h3 className="app__title">Diego Malatesta</h3>
      </header>
      <main className="app__main">
        {cards.map(card => <Card key={card.id} {...card} />)}
      </main>
    </>
  );
};

export default App;
